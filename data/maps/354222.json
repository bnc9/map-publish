{
  "header": {
    "title": "Severe Disabilities (354222) MAP Sheet",
    "department": "Department of Counseling Psychology and Special Education",
    "note": "For students entering the degree program during the 2015&mdash;2016 curricular year."
  },
  "headNotes": {
    "notes": {
      "markdown": "_This is a limited-enrollment program requiring departmental admissions approval. &nbsp;Please see the department office or college advisement center for information regarding requirements for admission to this program._\n\nThis major is designed to prepare students to teach in public schools. &nbsp;In order to graduate with this major, students are required to complete Utah State Office of Education licensing requirements. &nbsp;To view these requirements go to [http://education.byu.edu/ess/licensing.html](http://education.byu.edu/ess/licensing.html) or contact Education Student Services, 350 MCKB, (801) 422-3426\\."
    }
  },
  "firstPageFooter": {
    "footers": {
      "markdown": "**FOR UNIVERSITY CORE AND PROGRAM QUESTIONS CONTACT THE ADVISEMENT CENTER.**\n\n    ***** This class fills both university core and program requirements (3 hours overlap).\n\n    BS in SPECIAL EDUCATION: Severe Disabilities Emphasis (354222)\n\n    2015&mdash;2016"
    }
  },
  "secondPageNotes": {
    "notes": {
      "markdown": "**FRESHMAN YEAR**\n\n1st Semester\n\nFirst-Year Writing or A Htg 3.0\n\nASL 101 4.0\n\nBiology elective 3.0\n\nQuantitative Reasoning elective 3.0\n\nReligion Cornerstone course 2.0\n\n**Total Hours 15.0**\n\n2nd Semester\n\nFirst-year Writing or A Htg 3.0\n\nArts elective 3.0\n\nASL 102 4.0\n\nPhysical Science elective 3.0\n\nReligion Cornerstone course 2.0\n\n**Total Hours 15.0**\n\n**SOPHOMORE YEAR**\n\n3rd Semester\n\nCPSE 403 3.0\n\nASL 201 (recommended) 4.0\n\nCivilization 1 3.0\n\nReligion Cornerstone course 2.0\n\nSFL 210 (Social Science GE) 3.0\n\n**Total Hours &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 15.0**"
    }
  },
  "suggestedSequence": {
    "title": "Suggested Sequence of Courses",
    "notes": {
      "markdown": "To enter the professional sequence, apply by March 1st for the Fall semester. &nbsp;This should be done at the beginning of your fourth semester . &nbsp;Applications are completed online at\n\nmylink.byu.edu.\n\n4th Semester\n\nCPSE 425 3.0\n\nPETE 461 3.0\n\nASL 202 (Languages of Learning) 4.0\n\nMthEd 305 3.0\n\nReligion Cornerstone course 2.0\n\n**Total Hours 15.0**\n\n**Spring or Summer Term or Indep. Study**\n\nCivilization 2 elective 3.0\n\nLetters elective 3.0\n\nReligion elective 2.0\n\n**Total Hours 8.0**\n\nStudents must be admitted into the professional program before beginning the following classes."
    },
    "years": [
      {
        "yearTitle": "JUNIOR YEAR",
        "semesters": [
          {
            "title": "5th Semester",
            "suggestions": [
              { "text": "CPSE 410", "hours": 3 },
              { "text": "CPSE 420", "hours": 3 },
              { "text": "CPSE 463", "hours": 3 },
              { "text": "CPSE 480", "hours": 3 },
              { "text": "IP&amp;T 387", "hours": 1 },
              { "text": "Religion elective", "hours": 2 }
            ],
            "hours": 15
          },
          {
            "title": "6th Semester",
            "suggestions": [
              { "text": "CPSE 430", "hours": 3 },
              { "text": "CPSE 440", "hours": 2 },
              { "text": "CPSE 443", "hours": 3 },
              { "text": "CPSE 447R", "hours": 1 },
              { "text": "CPSE 460", "hours": 3 },
              { "text": "Engl 313", "hours": 3 },
              { "text": "IP&amp;T 487", "hours": 1 }
            ],
            "hours": 16
          },
          {
            "title": "Apply for student teaching or internship during your sixth semester. &nbsp;Applications should be completed online at mylink.byu.edu. The due date is February 15 th .",
            "suggestions": [
              { "text": "" },
              { "text": "Spring Term" },
              { "text": "CPSE 453", "hours": 3 },
              { "text": "CPSE 467R", "hours": 2 },
              { "text": "Religion elective", "hours": 2 }
            ],
            "hours": 7
          },
          {
            "title": "Summer Term",
            "suggestions": [{ "text": "CPSE 467R", "hours": 6 }, { "text": "CPSE 470", "hours": 3 }],
            "hours": 9
          }
        ]
      },
      {
        "yearTitle": "SENIOR YEAR",
        "semesters": [
          {
            "title": "7th Semester",
            "suggestions": [{ "text": "CPSE 487R (student teaching)", "hours": 12 }, { "text": "CPSE 490", "hours": 1 }],
            "hours": 13
          },
          {
            "title": "OR",
            "suggestions": [{ "text": "" }, { "text": "7th Semester" }, { "text": "CPSE 496R (internship)", "hours": 12 }],
            "hours": 12
          },
          {
            "title": "And",
            "suggestions": [{ "text": "" }, { "text": "8th Semester" }, { "text": "CPSE 490", "hours": 1 }],
            "hours": 1
          }
        ]
      }
    ]
  },
  "postSuggestedSequenceNotes": { "notes": { "markdown": "" } },
  "customSections": [
    {
      "sectionTitle": "THE DISCIPLINE:",
      "details": {
        "markdown": "Special Education is an important teaching discipline that emphasizes assessment, classification, and teaching to maximize the potential of students with disabilities. &nbsp;The undergraduate program prepares competent and moral educators who select, implement, and evaluate research-based effective teaching practices and appropriate curriculum for learners with special needs. &nbsp;Two areas of emphasis are available within the undergraduate program: students with mild/moderate disabilities and students with severe disabilities."
      }
    },
    {
      "sectionTitle": "CAREER OPPORTUNITIES:",
      "details": {
        "markdown": "A degree in Special Education leads to opportunities for teaching students with individualized educational needs in public and private schools, in grades K-12\\. &nbsp;&nbsp;A national shortage in qualified special education teachers provides an abundance of career opportunities."
      }
    }
  ],
  "secondPageFooter": {
    "footers": {
      "markdown": "**Counseling Psychology and Special Education Department**\n\n    340 MCKB, (801) 422-3857\n\n    [http://education.byu.edu/cpse](http://education.byu.edu/cpse)"
    }
  },
  "programRequirements": {
    "programText": [
      "This major is designed to prepare students to teach in public schools.  In order to graduate with this major, students are required to complete Utah State Office of Education licensing requirements.  To view these requirements go to http://education.byu.edu/ess/licensing.html or contact Education Student Services, 350 MCKB, (801) 422-3426.\n"
    ],
    "notes": [
      "For students accepted into the major after August 1, 2014, grades below C in any required coursework in a teaching major or teaching minor will not be accepted. Teacher candidates must maintain a total GPA of 3.0 or higher throughout the program and to qualify for student teaching. For details on admission and retention requirements for teaching majors and teaching minors, see Educator Preparation Program (EPP) Requirements in the undergraduate catalog.",
      "Student teachers/interns must complete three forms in their LiveText accounts (PIBS, CDS, FED) and attach their TWS to the LiveText account for their program.  All four must be completed to be cleared for graduation.",
      "Additional Licensing Requirements: It is the student's responsibility to be sure that the PRAXIS test has been passed and that BYU has received the test scores and that their fingerprint background clearance is current.  Students will also be responsible for any additional requirements imposed by the state prior to their graduation.  To confirm the status of these requirements, contact Education Student Services, 350 MCKB, (801) 422-3426.  Graduation and Utah licensure cannot be processed until these requirements have been completed."
    ],
    "requirements": [
      {
        "headerNotes": ["Complete the following with at least a B- grade prior to beginning the program:"],
        "label": { "heading": "Requirement 1", "completion": "Complete 2 Courses" },
        "courses": [
          {
            "courseTitle": "CPSE  403",
            "courseDescription": "Intro to Special Education",
            "curriculumId": "11751",
            "titleCode": "000",
            "credits": "3.0"
          },
          {
            "courseTitle": "PETE  461",
            "courseDescription": "Adapted/Multicultural PE Tchrs",
            "curriculumId": "04445",
            "titleCode": "010",
            "credits": "3.0"
          }
        ]
      },
      {
        "headerNotes": ["Core courses:"],
        "label": { "heading": "Requirement 2", "completion": "Complete 12 Courses" },
        "courses": [
          {
            "courseTitle": "CPSE  410",
            "courseDescription": "Appl Behavior Analysis in Ed",
            "curriculumId": "02562",
            "titleCode": "032",
            "credits": "3.0"
          },
          {
            "courseTitle": "CPSE  420",
            "courseDescription": "Assess/Eval Stdnts w/Excp Nds",
            "curriculumId": "02524",
            "titleCode": "008",
            "credits": "3.0"
          },
          {
            "courseTitle": "CPSE  425",
            "courseDescription": "Lang Arts Instru: Disabilities",
            "curriculumId": "11591",
            "titleCode": "000",
            "credits": "3.0"
          },
          {
            "courseTitle": "CPSE  430",
            "courseDescription": "Tching Read/LA Disabilities",
            "curriculumId": "02530",
            "titleCode": "019",
            "credits": "3.0"
          },
          {
            "courseTitle": "CPSE  440",
            "courseDescription": "C & I Sec Stnds w/Disabilities",
            "curriculumId": "10817",
            "titleCode": "000",
            "credits": "2.0"
          },
          {
            "courseTitle": "CPSE  460",
            "courseDescription": "Collaboration",
            "curriculumId": "11752",
            "titleCode": "000",
            "credits": "3.0"
          },
          {
            "courseTitle": "CPSE  470",
            "courseDescription": "Legal Issues in Spec Ed",
            "curriculumId": "10820",
            "titleCode": "000",
            "credits": "3.0"
          },
          {
            "courseTitle": "CPSE  480",
            "courseDescription": "Ed & Multicult Issues in Sp Ed",
            "curriculumId": "02565",
            "titleCode": "004",
            "credits": "3.0"
          },
          {
            "courseTitle": "CPSE  490",
            "courseDescription": "Capstone Sem: Ind w/Disab",
            "curriculumId": "10823",
            "titleCode": "000",
            "credits": "1.0"
          },
          {
            "courseTitle": "IP&T  387",
            "courseDescription": "Intgrtng Technology in SpEd 1",
            "curriculumId": "12110",
            "titleCode": "000",
            "credits": "1.0"
          },
          {
            "courseTitle": "IP&T  487",
            "courseDescription": "Intgrtng Technology in SpEd 2",
            "curriculumId": "12111",
            "titleCode": "000",
            "credits": "1.0"
          },
          {
            "courseTitle": "MTHED 305",
            "courseDescription": "Basic Conc of Math",
            "curriculumId": "10108",
            "titleCode": "001",
            "credits": "3.0"
          }
        ]
      },
      {
        "headerNotes": ["Emphasis courses:"],
        "label": { "heading": "Requirement 3", "completion": "Complete 2 Options" },
        "requirements": [
          {
            "label": { "heading": "Option 1", "completion": "Complete 6 Courses" },
            "courses": [
              {
                "courseTitle": "ASL   101",
                "courseDescription": "First-Year ASL 1",
                "curriculumId": "07742",
                "titleCode": "003",
                "credits": "4.0"
              },
              {
                "courseTitle": "ASL   102",
                "courseDescription": "First-Year ASL 2",
                "curriculumId": "07743",
                "titleCode": "003",
                "credits": "4.0"
              },
              {
                "courseTitle": "CPSE  443",
                "courseDescription": "Soc/Beh Strgs Severe Disabil",
                "curriculumId": "02529",
                "titleCode": "014",
                "credits": "3.0"
              },
              {
                "courseTitle": "CPSE  447R",
                "courseDescription": "Prac Sec Ed: Severe Disabil",
                "curriculumId": "02504",
                "titleCode": "010",
                "credits": "1.0",
                "rCourseLabel": "You may take this course up to 1 time"
              },
              {
                "courseTitle": "CPSE  453",
                "courseDescription": "Cur & Inst: Severe Disabilits",
                "curriculumId": "09872",
                "titleCode": "002",
                "credits": "3.0"
              },
              {
                "courseTitle": "CPSE  463",
                "courseDescription": "Asst Tech for Stdnts w/Disab",
                "curriculumId": "10819",
                "titleCode": "000",
                "credits": "3.0"
              }
            ]
          },
          {
            "label": { "heading": "Option 2", "completion": "Complete 8.0 credit hours from the following Courses" },
            "courses": [
              {
                "courseTitle": "CPSE  467R",
                "courseDescription": "Prac Tchg Stdnts w/ Severe Dis",
                "curriculumId": "01664",
                "titleCode": "006",
                "credits": "6.0v",
                "rCourseLabel": "You may take up to 8.0 credit hours"
              }
            ]
          }
        ]
      },
      {
        "headerNotes": ["Student teaching or internship experience:"],
        "footerNotes": [
          "Application for student teaching must be completed prior to student teaching.\n\nApplications are online at education.byu.edu/ess/application.html"
        ],
        "label": { "heading": "Requirement 4", "completion": "Complete 1 Option" },
        "requirements": [
          {
            "label": { "heading": "Option 1", "completion": "Complete 12.0 credit hours from the following Courses" },
            "courses": [
              {
                "courseTitle": "CPSE  487R",
                "courseDescription": "Student Tchg: Severe Disabilit",
                "curriculumId": "10822",
                "titleCode": "000",
                "credits": "12.0v",
                "rCourseLabel": "You may take up to 12.0 credit hours"
              },
              {
                "courseTitle": "CPSE  496R",
                "courseDescription": "Academic Internship: Spec Ed",
                "curriculumId": "02512",
                "titleCode": "006",
                "credits": "12.0v",
                "rCourseLabel": "You may take up to 12.0 credit hours"
              }
            ]
          }
        ]
      }
    ]
  },
  "meta": {
    "programCode": "354222",
    "programId": "33481",
    "description": "Severe Disabilities",
    "lastUpdated": "2016-05-26T16:50:42.991Z"
  }
}